from generate_data import generate_data
from model import DeepLearningModel
from file_paths import *

def main() -> None:
    # Descomenta la linea de abajo para generar los datos de entrenamiento
    #generate_data(with_augment=False)
    
    # El modelo es instanciado
    model = DeepLearningModel()
    
    # El primero modelo es poco profundo con el optimizador Adadelta
    model.build_shallow_model('Adadelta')
    model.train_model()
    model.save_model(PATH_SHALLOW_MODEL_ADADELTA)
    
    # El segundo modelo es profundo con el optimizador Adadelta
    model.build_deep_model('Adadelta')
    model.train_model()
    model.save_model(PATH_DEEP_MODEL_ADADELTA)
    
    # El tercer modelo es poco profundo con el optimizador SGD
    model.build_shallow_model('SGD')
    model.train_model()
    model.save_model(PATH_SHALLOW_MODEL_SGD)
    
    # El cuarto modelo es profundo con el optimizador SGD
    model.build_deep_model('SGD')
    model.train_model()
    model.save_model(PATH_DEEP_MODEL_SGD)

if __name__ == '__main__':
    main()