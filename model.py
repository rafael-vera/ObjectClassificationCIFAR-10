import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import to_categorical
from keras import backend as K
import numpy as np

from file_paths import *

class DeepLearningModel():
    """
    Atributos
    ----------
    OPTIMIZERS: dict
        Un diccionario con los algoritmos de optimizacion que pueden ser utilizados
    BATCH_SIZE: int
        El numero de ejemplos que seran pasados a traves de la red
    NUM_CLASSES: int
        El numero total de clases que tiene el conjunto de datos
    EPOCHS: int
        El numero de veces que el modelo sera entrenado
    
    Metodos
    -------
    build_shallow_model(optimizer: str)
        Sets a shallow model architecture with a given optimizer
    build_deep_model(optimizer: str)
        Sets a deep model architecture with a given optimizer
    train_model()
        The current model is trained and evaluated showing the training progress
    save_model(path: str)
        The current model is saved in the given path
    """
    
    OPTIMIZERS = {
        'Adadelta': tf.keras.optimizers.Adadelta(),
        'SGD': tf.keras.optimizers.SGD(lr=0.01, momentum=0.9)
    }
    
    BATCH_SIZE = 64
    NUM_CLASSES = 10
    EPOCHS = 60
    
    def __init__(self) -> None:
        """Los datos de entrenamiento y validacion son cargados
        """
        
        img_rows, img_cols = 32, 32
        
        self.x_train: np.ndarray = np.load(PATH_CIFAR10_TRAIN_IMAGES)
        self.y_train: np.ndarray = np.load(PATH_CIFAR10_TRAIN_LABELS)
        self.x_test: np.ndarray  = np.load(PATH_CIFAR10_TEST_IMAGES)
        self.y_test: np.ndarray  = np.load(PATH_CIFAR10_TEST_LABELS)
        
        if K.image_data_format() == 'channels_first':
            self.x_train = self.x_train.reshape(
                self.x_train.shape[0], 3, img_rows, img_cols
            )
            self.x_test = self.x_test.reshape(
                self.x_test.shape[0], 3, img_rows, img_cols
            )
            self.input_shape = (3, img_rows, img_cols)
        else:
            self.x_train = self.x_train.reshape(
                self.x_train.shape[0], img_rows, img_cols, 3
            )
            self.x_test = self.x_test.reshape(
                self.x_test.shape[0], img_rows, img_cols, 3
            )
            self.input_shape = (img_rows, img_cols, 3)
        
        self.x_train = self.x_train.astype('float32')
        self.x_train /= 255
        
        self.x_test  = self.x_test.astype('float32')
        self.x_test /= 255
        
        self.y_train = to_categorical(self.y_train, self.NUM_CLASSES)
        self.y_test  = to_categorical(self.y_test, self.NUM_CLASSES)
    
    def build_shallow_model(self, optimizer: str) -> None:
        """Se genera un modelo con arquitectura poco profunda y el optimizador dado.
        
        El modelo se genera con la siguiente arquitectura:
            1 capa Conv2D con 32 filtros, un kernel 3x3 y activacion relu.
            1 capa Conv2D con 64 filtros, un kernel 3x3 y activacion relu.
            1 capa de MaxPooling con un tamaño de 2x2.
            Se aplana la salida del MaxPooling.
            1 capa de neuronas (Dense) con 128 salidas y activacion relu.
            1 capa de neuronas (Dense) con 10 salidas (1 por clase) y activacion softmax.
        
        Parametros
        ----------
        optimizer: str
            El algoritmo utilizado para reducir la funcion de perdida
        """
        
        self.model = Sequential()
        self.model.add(
            Conv2D(32, kernel_size=(3,3), activation='relu', input_shape=self.input_shape)
        )
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        self.model.add(Flatten())
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(self.NUM_CLASSES, activation='softmax'))
        
        self.model.compile(
            loss=keras.losses.categorical_crossentropy,
            optimizer=self.OPTIMIZERS[optimizer],
            metrics=['accuracy']
        )
    
    def build_deep_model(self, optimizer: str) -> None:
        """Se genera un modelo con arquitectura profunda y el optimizador dado.
        
        El modelo se genera con la siguiente arquitectura:
            1 capa Conv2D con 32 filtros, un kernel 3x3 y activacion relu.
            4 capas Conv2D con 64 filtros, un kernel 3x3 y activacion relu.
            1 capa de MaxPooling con un tamaño de 2x2.
            Se aplana la salida del MaxPooling.
            2 capas de neuronas (Dense) con 128 salidas y activacion relu.
            1 capa de neuronas (Dense) con 10 salidas (1 por clase) y activacion softmax.
        
        Parametros
        ----------
        optimizer: str
            El algoritmo utilizado para reducir la funcion de perdida
        """
        self.model = Sequential()
        self.model.add(
            Conv2D(32, kernel_size=(3,3), activation='relu', input_shape=self.input_shape)
        )
        
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        self.model.add(Flatten())
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(128, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(self.NUM_CLASSES, activation='softmax'))
        
        self.model.compile(
            loss=keras.losses.categorical_crossentropy,
            optimizer=self.OPTIMIZERS[optimizer],
            metrics=['accuracy']
        )
    
    def train_model(self) -> None:
        """El actual modelo es entrenado y evaluado mostrando el proceso de entrenamiento.
        """
        
        # Se muestra información referente al modelo creado
        print(f'Model parameters = {self.model.count_params()}')
        print(self.model.summary())
        
        # Se entrena el modelo
        history = self.model.fit(
            self.x_train, self.y_train,
            batch_size=self.BATCH_SIZE,
            epochs=self.EPOCHS,
            verbose=1,
            validation_data=(self.x_test[:1000], self.y_test[:1000])
        )
        
        # Se evalua el modelo
        score = self.model.evaluate(self.x_test[1000:], self.y_test[1000:], verbose=0)
        
        # Se muestra los resultados de la evaluacion
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
    
    def save_model(self, path: str) -> None:
        """El actual modelo se guarda en la ruta especificada.
        
        Parametros
        ----------
        path: str
            La ruta donde el modelo se guardara
        """
        
        self.model.save(path)