# Rutas donde se almacenan los datos generados y procesados para su uso
PATH_CIFAR10_TRAIN_IMAGES = 'datasets/cifar10_train_images.npy'
PATH_CIFAR10_TRAIN_LABELS = 'datasets/cifar10_train_labels.npy'
PATH_CIFAR10_TEST_IMAGES  = 'datasets/cifar10_test_images.npy'
PATH_CIFAR10_TEST_LABELS  = 'datasets/cifar10_test_labels.npy'

# Rutas donde se almacenan los conjuntos de datos aumentados
PATH_CIFAR10_AUGMENTED_TRAIN_IMAGES = 'datasets/cifar10_augmented_train_images.npy'
PATH_CIFAR10_AUGMENTED_TRAIN_LABELS = 'datasets/cifar10_augmented_train_labels.npy'
PATH_CIFAR10_AUGMENTED_TEST_IMAGES  = 'datasets/cifar10_augmented_test_images.npy'

# Rutas donde se almacenan los modelos entrenados
PATH_SHALLOW_MODEL_ADADELTA = 'models/shallow_model_adadelta.h5'
PATH_DEEP_MODEL_ADADELTA    = 'models/deep_model_adadelta.h5'
PATH_SHALLOW_MODEL_SGD      = 'models/shallow_model_sgd.h5'
PATH_DEEP_MODEL_SGD         = 'models/deep_model_sgd.h5'